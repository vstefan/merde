//g++ -std=c++20 -Wall -Werror seed_seed.cpp 

#include <iostream>
#include <random>

using namespace std;


void gen_rnd_seq(uint32_t seed) 
{
    mt19937 gen(seed);
    uniform_int_distribution<> distrib(0,100);

    for(int i=0;i<10;i++)
	cout << distrib(gen) << " ";
    
    
    cout << endl;
}


int main() 
{
    //random_device rd;  //Will be used to obtain a seed for the random number engine
    //mt19937 gen(rd()); //Standard mersenne_twister_engine seeded
    //with rd()
    mt19937 gen(1234);
    uniform_int_distribution<> distrib;

    for(int i=0; i< 10; i++) 
	gen_rnd_seq(distrib(gen));    

    return 0;
}
