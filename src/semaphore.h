#include <mutex>
#include <condition_variable>

using namespace std;


/*
 * Semaphore 
 * 
 * Lightweight synchronisation class to controll number of simultneous running threads.
 * @param count maximum of simultaniosly acquired 
 *
 */

class Semaphore {
    mutex mtx_;
    condition_variable cv_;
    unsigned count_;
public:
    Semaphore (unsigned count=0) : count_(count) { }
    
    void release() {
        scoped_lock ls(mtx_);
        count_++;
        cv_.notify_one();
    }

    void acquire() {
        unique_lock lu(mtx_);
        cv_.wait(lu, [this] { return this->count_; });
        count_--;
    }
};
