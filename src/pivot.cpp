#include "pivot.h"
#include "container.h"
#include <iostream>
#include <random>
#include <chrono>
#include <cmath>
#include <algorithm>

long int best_imp(PfspInstance& pfsp, Neighborhood* n, vector<int>& sol)
{
	bool improvement = true;
	
	long int score = pfsp.computeWCT(sol); //score of the initial solution

	while (improvement) //continue looping as long as we can improve the solution
	{
		vector<vector<int>> neighbors;
		improvement = false;
		n->getNeighbors(sol, neighbors); //get all the neighbours of the current state
		for  (vector<int> var : neighbors) // check all the neighbours
		{
			int tmp = pfsp.computeWCT(var);
			//replace the current best solution if we find a better one
			if (tmp < score) { score = tmp; sol = var; improvement = true; }
		}
	}
	return score;
}

long int best_imp_WCT_matrix(PfspInstance& pfsp, Neighborhood* n, vector<int>& sol)
{
	bool improvement = true;

	long int score = pfsp.computLastingWCT(sol, 1);//compute the score and keep track of the computation
	vector<int> last;// Contains the last permutation whose WCT was computed
	last = sol;
	
	while (improvement) //continue looping as long as we can improve the solution
	{
		vector<vector<int>> neighbors;
		improvement = false;
		n->getNeighbors(sol, neighbors); //get all the neighbours of the current state
		for (vector<int> var : neighbors) // check all the neighbours
		{
			//find the first difference between the current permutation and the last one whose score was computed
			int start = firstDiff(last, var);
			int tmp = pfsp.computLastingWCT(var, start);//Recompute only the necessary elements
			last = var;
			//replace the current best solution if we find a better one
			if (tmp < score) { score = tmp; sol = var; improvement = true; }
		}
	
		
	}

	return score;
}

long int first_imp(PfspInstance& pfsp, Neighborhood* n, vector<int>& sol)
{
	bool improvement = true;
	long int score = pfsp.computeWCT(sol);
	
	while (improvement) //continue looping as long as we can improve the solution
	{
		vector<vector<int>> neighbors;
		improvement = false;
		n->getNeighbors(sol, neighbors); //get all the neighbours of the current state
		for (vector<int> var : neighbors)
		{
			int tmp = pfsp.computeWCT(var);
			//replace the current best solution if we find a better one and end the current loop 
			if (tmp < score) { score = tmp; sol = var; improvement = true; break;}
		}
		
	}
	
	return score;
}

long int first_imp_WCT_matrix(PfspInstance& pfsp, Neighborhood* n, vector<int>& sol)
{
	bool improvement = true;
	long int score = pfsp.computLastingWCT(sol, 1); //compute the score and keep track of the computation
	vector<int> last;// Contains the last permutation whose WCT was computed
	last = sol;

	while (improvement) //continue looping as long as we can improve the solution
	{
		vector<vector<int>> neighbors;
		improvement = false;
		n->getNeighbors(sol, neighbors); //get all the neighbours of the current state
		for (vector<int> var : neighbors)
		{
			//find the first difference between the current permutation and the last one whose score was computed
			int start = firstDiff(last, var); 
			int tmp = pfsp.computLastingWCT(var, start); //Recompute only the necessary elements
			last = var;
			//replace the current best solution if we find a better one and end the current loop 
			if (tmp < score) { score = tmp; sol = var; improvement = true; break; }
		}

	}

	return score;
}

long int vnd(PfspInstance& pfsp, Neighborhood** n, vector<int>& sol)
{
	long int score = pfsp.computLastingWCT(sol, 1);
	vector<int> sol_ = sol;
	int i = 0;
	while (i < 3)
	{
		//apply first improvement until a local optimu is reached
		int tmp = first_imp_WCT_matrix(pfsp, n[i], sol_);
		if (tmp < score) 
		{
			score = tmp; 
			sol = sol_; //keep track of the best soltion
			i = 0;
		}
		else //If there is no improvement, change neighborhood
		{
			i++;
		}
	}
	return score;
}

int firstDiff(vector<int>& v1, vector<int>& v2)
{
	int i;
	int l = v1.size() - 1;
	for (i = 1; i <= l; ++i)
	{
		if (v1[i] != v2[i])
		{
			break;
		}
	}
	return i;
}

long int ils(PfspInstance& pfsp, vector<int>& sol, uint32_t seed)
{
	/*
	* 1.generate initial solution <- done outside
	* 2.local search
	* 3.repeat until termination criterion
	*	3.1.perurbation
	*	3.2. local search
	*	3.3.Acceptation criterion
	*/
	long long total_duration = 0;
	if (pfsp.getNbJob() == 50) { total_duration = 150000000; }
	else { total_duration = 350000000; }
	long long duration;
	double lambda = 4.0; 
	double temperature = lambda*pfsp.getTotalTime()/(10*(double)pfsp.getNbJob()* (double)pfsp.getNbMac());
	int gamma = 2; 
	InsertNeighborhood n = InsertNeighborhood();
	long int bestScore = best_imp_WCT_matrix(pfsp, &n, sol);
	vector<int> sol_ = sol; //current considered solution
	vector<int> lastSol = sol; //last seach position
	long int lastScore = bestScore;

	std::mt19937 gen(seed);
	std::uniform_real_distribution<double> dis_real(0.0, 1.0);

	auto startPoint = std::chrono::high_resolution_clock::now();
	auto start = std::chrono::time_point_cast<std::chrono::microseconds>(startPoint).time_since_epoch().count();
	do
	{
        perturbation(sol_, gamma, gen);
		int score = best_imp_WCT_matrix(pfsp, &n, sol_);
		if (score < lastScore)//accept obtained solution if it is better
		{
			lastSol = sol_;
			lastScore = score;
			if (lastScore < bestScore)//Replace best-known solution if necessary
			{
				sol = lastSol;
				bestScore = lastScore;
			}
		}
		//accept a worst solution with a given probability
		else if(dis_real(gen) <= exp(((double)lastScore - (double)score)/temperature) )
		{
			lastSol = sol_;
			lastScore = score;
		}
		//Return to the current search position
		else
		{
			sol_ = lastSol;
		}
		auto endPoint = std::chrono::high_resolution_clock::now();
		auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endPoint).time_since_epoch().count();
		duration = end - start;
	} while (duration <= total_duration);
	return bestScore;
}

void perturbation(vector<int>& sol, const int gamma, mt19937& gen)
{

	std::uniform_int_distribution<> dis(1, sol.size() - 1);
	//perform gamma random insertions
	for (int n = 0; n < gamma; ++n) {
		int start = dis(gen);
		int dst = dis(gen);
		move_element(start, dst, sol);
	}
}

void ils_run_time(PfspInstance& pfsp, vector<int>& sol, vector<Container>& dist, uint32_t seed)
{
	/*
	* 1.generate initial solution <- done outside
	* 2.local search
	* 3.repeat until termination criterion
	*	3.1.perurbation
	*	3.2. local search
	*	3.3.Acceptation criterion
	*/
	long long total_duration = 1000000000;
	long long duration;
	double lambda = 4.0;
	double temperature = lambda * pfsp.getTotalTime() / (10 * (double)pfsp.getNbJob() * (double)pfsp.getNbMac());
	int gamma = 2;
	InsertNeighborhood n = InsertNeighborhood();
	long int bestScore = best_imp_WCT_matrix(pfsp, &n, sol);
	vector<int> sol_ = sol; //current solution being considered
	vector<int> lastSol = sol; //last search position
	long int lastScore = bestScore;

	std::mt19937 gen(seed);
	std::uniform_real_distribution<double> dis_real(0.0, 1.0);

	auto startPoint = std::chrono::high_resolution_clock::now();
	auto start = std::chrono::time_point_cast<std::chrono::microseconds>(startPoint).time_since_epoch().count();
	do
	{
        perturbation(sol_, gamma, gen);
		int score = best_imp_WCT_matrix(pfsp, &n, sol_);
		if (score < lastScore)//accept obtained solution if it is better
		{
			lastSol = sol_;
			lastScore = score;
			if (lastScore < bestScore) //replace best-known solution if necessary
			{
				sol = lastSol;
				bestScore = lastScore;
				//keep track of an improvement to the best known search position
				auto endPoint = std::chrono::high_resolution_clock::now();
				auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endPoint).time_since_epoch().count();
				duration = end - start;
				dist.push_back(Container(duration, bestScore));
			}
		}
		//accept a worst solution with a given probability
		else if (dis_real(gen) <= exp(((double)lastScore - (double)score) / temperature))
		{
			lastSol = sol_;
			lastScore = score;
		}
		//Return to the current search position
		else
		{
			sol_ = lastSol;
		}
		auto endPoint = std::chrono::high_resolution_clock::now();
		auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endPoint).time_since_epoch().count();
		duration = end - start;
	} while (duration <= total_duration);
}

long int sa(PfspInstance& pfsp, vector<int>& sol, uint32_t seed) {
	//set-up temperature and cooling schedule
	long long total_duration = 0;
	int N = 0;
	if (pfsp.getNbJob() == 50) { N = 83565000; total_duration = 150000000; }
	else { N = 194985000; total_duration = 350000000;}
	double temperature = 4 * (double)pfsp.getTotalTime();
	double beta = (temperature - 1)/(temperature*(N-1));

	long int bestScore = pfsp.computLastingWCT(sol, 1);//keep in memory the best score so far
	vector<int> sol_ = sol; //current search position
	long int score_ = bestScore; //score of the current search position
	vector<int> last = sol; //solution candidate whose score was computed last
	InsertNeighborhood n = InsertNeighborhood();

	vector<vector<int> > neighbors;
	n.getNeighbors(sol_, neighbors);
	vector<int> neighborSol = sol; //last solution candidate that had its neighbors computed
	//set up a random number generator necessary since rand() is not thread safe
	std::mt19937 gen(seed);
	
	std::uniform_int_distribution<> dis(0, neighbors.size() - 1);
	std::uniform_real_distribution<double> dis_real(0.0, 1.0);
	auto startPoint = std::chrono::high_resolution_clock::now();
	auto start = std::chrono::time_point_cast<std::chrono::microseconds>(startPoint).time_since_epoch().count();
	long long duration;
	do
	{
		if (neighborSol != sol_)//don't recompute the neighbors if the search position didn't change
		{
			neighbors.resize(0);
			n.getNeighbors(sol_, neighbors);
			neighborSol = sol_;
		}
		//Choose a random neighbor
		int rn = dis(gen);
		int first = firstDiff(last, neighbors[rn]);
		long int score = pfsp.computLastingWCT(neighbors[rn], first);
		last = neighbors[rn];
		if (score < score_)//accept obtained solution if it is better
		{
			neighborSol = sol_;
			sol_ = neighbors[rn];
			score_ = score;
			if (score_ < bestScore) //replace best-known solution if necessary
			{
				sol = sol_;
				bestScore = score_;
			}
		}
		//accept a worst solution with a given probability
		else if (dis_real(gen) <= exp(((double)score_ - (double)score)/temperature))
		{
			neighborSol = sol_;
			sol_ = neighbors[rn];
			score_ = score;
		}
		auto endPoint = std::chrono::high_resolution_clock::now();
		auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endPoint).time_since_epoch().count();
		duration = end - start;
		//compute the new temperature
		temperature = max(temperature/(1 + beta * temperature),1.0);
	} while (duration <= total_duration);
	return bestScore;
}


void sa_run_time(PfspInstance& pfsp, vector<int>& sol, vector<Container>& dist, uint32_t seed) {
	//set-up temperature and cooling schedule
	long long total_duration = 1000000000;
	int N = 557100000;
	double temperature = 4*(double)pfsp.getTotalTime();
	double beta = (temperature - 1) / (temperature * (N - 1));

	long int bestScore = pfsp.computLastingWCT(sol, 1);//keep in memory the best score so far
	vector<int> sol_ = sol; //current search position
	long int score_ = bestScore; //score of the current search position
	vector<int> last = sol; //solution candidate whose score was computed last
	InsertNeighborhood n = InsertNeighborhood();

	vector<vector<int> > neighbors;
	n.getNeighbors(sol_, neighbors);
	vector<int> neighborSol = sol; //last solution candidate that had its neighbors computed

	//set up a random number generator necessary since rand() is not thread safe
	std::mt19937 gen(seed);
	std::uniform_int_distribution<> dis_int(0, neighbors.size() - 1);
	std::uniform_real_distribution<double> dis_real(0.0, 1.0);
	auto startPoint = std::chrono::high_resolution_clock::now();
	auto start = std::chrono::time_point_cast<std::chrono::microseconds>(startPoint).time_since_epoch().count();
	long long duration;
	do
	{
		if (neighborSol != sol_)//don't recompute the neighbors if the search position didn't change
		{
			neighbors.resize(0);
			n.getNeighbors(sol_, neighbors);
			neighborSol = sol_;
		}
		//Choose a random neighbor
		
		int rn = dis_int(gen);
		int first = firstDiff(last, neighbors[rn]);
		long int score = pfsp.computLastingWCT(neighbors[rn], first);
		last = neighbors[rn];
		if (score < score_)//accept obtained solution if it is better
		{
			neighborSol = sol_;
			sol_ = neighbors[rn];
			score_ = score;
			if (score_ < bestScore) //replace best-known search position if necessary
			{
				sol = sol_;
				bestScore = score_;
				//Keep track of all improvements to the best-known solution
				auto endPoint = std::chrono::high_resolution_clock::now();
				auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endPoint).time_since_epoch().count();
				duration = end - start;
				dist.push_back(Container(duration, bestScore));
			}
		}
		//accept a worst solution with a given probability
		else if (dis_real(gen) <= exp(((double)score_ - (double)score) / temperature))
		{
			neighborSol = sol_;
			sol_ = neighbors[rn];
			score_ = score;
		}
		auto endPoint = std::chrono::high_resolution_clock::now();
		auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endPoint).time_since_epoch().count();
		duration = end - start;
		//compute the new temperature
		
		temperature = max(temperature / (1 + beta * temperature), 1.0);
	} while (duration <= total_duration);
}
