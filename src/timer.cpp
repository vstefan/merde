#include "timer.h"

Timer::Timer(std::vector<long long>* durations) :
    v(durations),
    startPoint(std::chrono::high_resolution_clock::now()) //start the timer
{

}

Timer::~Timer()
{
	stop();
}

void Timer::stop()
{
	auto endPoint = std::chrono::high_resolution_clock::now(); //stop the timer

	//Compute the total duration and store in a vector
	auto start = std::chrono::time_point_cast<std::chrono::microseconds>(startPoint).time_since_epoch().count();
	auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endPoint).time_since_epoch().count();
	auto duration = end - start;
	v->push_back(duration);
}
