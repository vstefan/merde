#include "solve.h"
#include "timer.h"
#include "pivot.h"

int solve(void(*init_sol)(std::vector<int>&, PfspInstance&), Neighborhood* n,
	  long int(*pivot)(PfspInstance&, Neighborhood*, std::vector<int>&), ofstream& fout1, const string& fileName)
{
    int inst = 30; // number of instances of a given type
    long int score;
    vector<long long> durations; //vector keeping track of all the execution times for each pfsp instance
    std::vector<int> solution;
    PfspInstance instance;
    string name;
    for (int i = 1; i <= 9; ++i)
    {
        /* 1. Read data from file */
        name = fileName + "0" + to_string(i);
        if (!instance.readDataFromFile(name)) { return 1; }
        solution.resize(instance.getNbJob() + 1);
        {
            /* 2. Start the timer*/
            Timer timer = Timer(&durations);
            /* 3. Build initial solution */
            (*init_sol)(solution, instance);
            /* 4. Apply iterative improvement*/
            score = (*pivot)(instance, n, solution);
        }
        fout1 << durations[i - 1] << " , " << score << "\n";
    }
    for (int i = 10; i <= inst; ++i)
    {
        /* 1. Read data from file */
        name = fileName + to_string(i);
        if (!instance.readDataFromFile(name)) { return 1; }
        solution.resize(instance.getNbJob() + 1);
        {
            /* 2. Start the timer*/
            Timer timer = Timer(&durations);
            /* 3. Build initial solution */
            (*init_sol)(solution, instance);
            /* 4. Apply iterative improvement*/
            score = (*pivot)(instance, n, solution);
        }
        fout1 << durations[i - 1] << " , " << score << "\n";
    }
    return 0;
}

int solve(void(*init_sol)(std::vector<int>&, PfspInstance&), Neighborhood** n, ofstream& fout1, const string& fileName)
{
    int inst = 30; // number of instances of a given type
    long int score;
    vector<long long> durations; //vector keeping track of all the execution times for each pfsp instance
    std::vector<int> solution;
    PfspInstance instance;
    string name;
    /* Read data from file */
    for (int i = 1; i <= 9; ++i)
    {
        /* 1. Read data from file */
        name = fileName + "0" + to_string(i);
        if (!instance.readDataFromFile(name)) { return 1; }
        solution.resize(instance.getNbJob() + 1);
        {
            /* 2. Start the timer*/
            Timer timer = Timer(&durations);
            /* 3. Build initial solution */
            (*init_sol)(solution, instance);
            /* 4. Apply iterative improvement*/
            score = vnd(instance, n, solution);
        }
        fout1 << durations[i - 1] << " , " << score << "\n";
    }
    for (int i = 10; i <= inst; ++i)
    {
        /* 1. Read data from file */
        name = fileName + to_string(i);
        if (!instance.readDataFromFile(name)) { return 1; }
        solution.resize(instance.getNbJob() + 1);
        {
            /* 2. Start the timer*/
            Timer timer = Timer(&durations);
            /* 3. Build initial solution */
            // Fill the vector with a random permutation
            (*init_sol)(solution, instance);
            /* 4. Apply iterative improvement*/
            score = vnd(instance, n, solution);
        }
        fout1 << durations[i - 1] << " , " << score << "\n";
    }
    return 0;
}
