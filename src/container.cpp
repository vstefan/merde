#include "container.h"

Container::Container(long long t, long int q)
{
	time = t;
	quality = q;
}

Container::~Container() { }


void Container::setTime(long long t)
{
	time = t;
}

void Container::setQuality(long int q)
{
	quality = q;
}

long long Container::getTime()
{
	return time;
}

long int Container::getQuality()
{
	return quality;
}
