#ifndef _INITSOL_
#define _INITSOL_


#include <vector>
#include "pfspinstance.h"

/**
* Create an initial solution using the simple RZ heuristic
* @param sol Output vector that will contain the initial solution
* @param pfsp Current instance of the pfsp
*/
void simpleRZ(std::vector<int>& sol, PfspInstance& pfsp);

int generateRndPosition(int min, int max);

void randomPermutation(vector< int >& sol, PfspInstance& pfsp);

#endif // !INITSOL