#ifndef _CONTAINER_H_
#define _CONTAINER_H_

/**
* Class used to keep track of found for the pfsp during the application of an SLS algorithm
*/
class Container
{
public:
	Container(long long t, long int q);
	~Container();

	void setTime(long long t);
	void setQuality(long int q);

	long long getTime();
	long int getQuality();

private:
	long long time;
	long int quality;
};
#endif