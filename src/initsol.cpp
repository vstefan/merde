#include "initsol.h"
#include <tuple>
#include <iostream>
#include <vector>
#include "neighborhood.h"

void simpleRZ(std::vector<int>& sol, PfspInstance& pfsp)
{
	//create a vector of tuples containing <weighted processing time of a job, job number>
	//the vector is sorted in ascenting order of processing time
	vector < tuple<float, int>> weightedTime(pfsp.getNbJob() + 1);
	pfsp.weightedProcessingTime(weightedTime);
	
	//Create a solution by inserting each element of the previously generated vector in
	//a position that minimizes the cost function
	sol[1] = get<1>(weightedTime.at(1)); //insert the first element
	int nbJobs = pfsp.getNbJob();
	for (int i = 2; i <= nbJobs; i++)
	{
		vector<int> n = sol;
		n[i] = get<1>(weightedTime.at(i));//insert element i at the last available position
		long int best_score = pfsp.computePartialWCT(n, i);//compute the score
		vector<int> best_sol = n;
		
		for (int j = 1; j <=  i - 1; j++)//Try to insert the element in all other positions
		{
			vector<int> neighbor = n;
			move_element(i, j, neighbor);
			int long score = pfsp.computePartialWCT(neighbor, i); //compute the score
			if (score < best_score)//keep track of the best solution so far
			{
				best_score = score;
				best_sol = neighbor;
			}
		}
		sol = best_sol;
	}
	
}

int generateRndPosition(int min, int max)
{
	return (rand() % max + min);
}

void randomPermutation(vector< int >& sol, PfspInstance& pfsp) {
	int nbJobs = pfsp.getNbJob();

	vector<bool> alreadyTaken(nbJobs + 1, false); // nbJobs elements with value false
	vector<int > choosenNumber(nbJobs + 1, 0);

	int nbj = 0;
	for (int i = nbJobs; i >= 1; --i)
	{
		int rnd = generateRndPosition(1, i);
		int nbFalse = 0;

		/* find the rndth cell with value = false : */
		int j;
		for (j = 1; nbFalse < rnd; ++j)
			if (!alreadyTaken[j])
				++nbFalse;
		--j;

		sol[j] = i;

		++nbj;
		choosenNumber[nbj] = j;

		alreadyTaken[j] = true;
	}
}
