/***************************************************************************
 *   Copyright (C) 2012 by Jérémie Dubois-Lacoste   *
 *   jeremie.dl@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <tuple>
#include <algorithm>
#include <string>
#include <sstream>
#include <cerrno>
#include <regex>

#include "pfspinstance.h"

using namespace std;

PfspInstance::PfspInstance() : nbJob(0), nbMac(0)
{
	
}


PfspInstance::~PfspInstance()
{
}

int PfspInstance::getNbJob()
{
  return nbJob;
}

int PfspInstance::getNbMac()
{
  return nbMac;
}



/* Allow the memory for the processing times matrix : */
void PfspInstance::allowMatrixMemory(int nbJ, int nbM)
{
	processingTimesMatrix.resize(nbJ+1);          // 1st dimension
	matrixWCT.resize(nbJ + 1);

	for (int cpt = 0; cpt < nbJ + 1; ++cpt) {
		processingTimesMatrix[cpt].resize(nbM + 1); // 2nd dimension
		matrixWCT[cpt].resize(nbM + 1);
	}
	dueDates.resize(nbJ+1);
	priority.resize(nbJ+1);

	//fill the matrix used to compute the WCT with 0s on the first column
	for (int i = 0; i < nbJ + 1; i++)
	{
		matrixWCT[i][0] = 0;
	}
	//fill the matrix used to compute the WCT with 0s on the first row
	for (int i = 0; i < nbM + 1; i++)
	{
		matrixWCT[0][i] = 0;
	}
}


long int PfspInstance::getTime(int job, int machine)
{
  if (job == 0)
    return 0;
  else
  {
    if ((job > nbJob) || (machine < 1) || (machine > nbMac))
      cout    << "ERROR. file:pfspInstance.cpp, method:getTime. Out of bound. job=" << job
          << ", machine=" << machine << std::endl;

    return processingTimesMatrix[job][machine];
  }
}

long int PfspInstance::getDueDate(int job)
{
	return dueDates[job];
}

void PfspInstance::setDueDate(int job, int value)
{
	dueDates[job] = value;
}

long int PfspInstance::getPriority(int job)
{
	return priority[job];
}

void PfspInstance::setPriority(int job, int value)
{
	priority[job] = value;
}

/* 
 * returns the filename (text after last /) from a full path
 *
 * TODO: find right place for this function
 */
string filename(const string& path) 
{
    constexpr char seperator = '/';
    
    size_t pos = 0;
    while(path.find(seperator, pos+1) != string::npos) 
	pos = path.find(seperator, pos+1);

    return path.substr(pos+1);
}



/* Read the instance from file : */
bool PfspInstance::readDataFromFile(const string& filePath)
{
	bool everythingOK = true;
	const string fileName = filename(filePath);
	
	//check pattern is matched
	const regex fname_pattern("[0-9]+_[0-9]+_[0-9]+");
	smatch fmatch;
	if(!regex_match(fileName, fmatch, fname_pattern)) {
		cout << "ERROR. file:pfspInstance.cpp, method:readDataFromFile, "
			 << "invalid filename " << fileName << endl;

		return false;
	}

	//open the file
	string str;
	ifstream fileIn(filePath);
	if (fileIn) {
		fileIn >> nbJob;
		fileIn >> nbMac;
		allowMatrixMemory(nbJob, nbMac);
		
		cout << "Processing file: " << fileName << " with jobs: " << nbJob << ", machines: " << nbMac << endl;

		for (int j = 1; j <= nbJob; ++j)
		{
			for (int m = 1; m <= nbMac; ++m)
			{
			        long int readValue;
				fileIn >> readValue; // The number of each machine, not important !
				fileIn >> readValue; // Process Time

				processingTimesMatrix[j][m] = readValue;
			}
		}
		fileIn >> str; // this is not read

		for (int j = 1; j <= nbJob; ++j)
		{
		        long int readValue;
			fileIn >> readValue; // -1
			fileIn >> readValue;
			dueDates[j] = readValue;
			fileIn >> readValue; // -1
			fileIn >> readValue;
			priority[j] = readValue;
		}

		cout << "All is read from file." << std::endl;
		fileIn.close();
	}
	else
	{
		cout << "ERROR. file:pfspInstance.cpp, method:readDataFromFile, "
			<< "error while opening file " << fileName << endl;

		return false;
	}

	return everythingOK;
}

long int PfspInstance::getTotalTime()
{
	long int total = 0;
	for (int i = 1; i <= nbJob; i++)
	{
		for (int j = 1; j <= nbMac; j++)
		{
			total += processingTimesMatrix[i][j];
		}
	}
	return total;
}

void PfspInstance::weightedProcessingTime(vector<tuple<float, int> >& weightedTime)
{
	for (int i = 1; i <= nbJob; ++i)
	{
		//computed the weighted sum of processing times for each job
		float processingTime = 0;
		for (int j = 1; j <= nbMac; ++j)
		{
			processingTime += processingTimesMatrix[i][j];
		}
		processingTime /= priority[i];

		// create a tuple containing (weighted sum of processing time, job number)
		weightedTime[i] = make_tuple(processingTime, i);
	}
	//Sort all jobs in ascending order of processing time
	sort(weightedTime.begin() + 1, weightedTime.end());
}


/* Compute the weighted sum of completion time of a given solution */
long int PfspInstance::computeWCT(const vector< int > & sol)
{
	int j, m;
	int jobNumber;
	long int wct;

	/* We need end times on previous machine : */
	vector< long int > previousMachineEndTime ( nbJob + 1 );

	/* 1st machine : */
	previousMachineEndTime[0] = 0;
	for ( j = 1; j <= nbJob; ++j )
	{
		jobNumber = sol[j];
		previousMachineEndTime[j] = previousMachineEndTime[j-1] + processingTimesMatrix[jobNumber][1];
	}

	/* others machines : */
	for ( m = 2; m <= nbMac; ++m )
	{
		previousMachineEndTime[1] +=
				processingTimesMatrix[sol[1]][m];
		/* And the end time of the previous job, on the same machine : */
		long int previousJobEndTime = previousMachineEndTime[1];


		for ( j = 2; j <= nbJob; ++j )
		{
			jobNumber = sol[j];

			if ( previousMachineEndTime[j] > previousJobEndTime )
			{
				previousMachineEndTime[j] = previousMachineEndTime[j] + processingTimesMatrix[jobNumber][m];
				previousJobEndTime = previousMachineEndTime[j];
			}
			else
			{
				previousJobEndTime += processingTimesMatrix[jobNumber][m];
				previousMachineEndTime[j] = previousJobEndTime;
			}
		}
	}

	wct = 0;
	for ( j = 1; j<= nbJob; ++j )
	    wct += previousMachineEndTime[j] * priority[sol[j]];

	return wct;
}

long int PfspInstance::computePartialWCT(const vector<int>& sol, int partialJobNb)
{
	int j, m;
	int jobNumber;
	long int wct;
	/* We need end times on previous machine : */
	vector< long int > previousMachineEndTime(partialJobNb + 1);
	
	/* 1st machine : */
	previousMachineEndTime[0] = 0;
	for (j = 1; j <= partialJobNb; ++j)
	{
		jobNumber = sol[j];
		previousMachineEndTime[j] = previousMachineEndTime[j - 1] + processingTimesMatrix[jobNumber][1];
	}

	/* others machines : */
	for (m = 2; m <= nbMac; ++m)
	{
		previousMachineEndTime[1] +=
			processingTimesMatrix[sol[1]][m];
		/* And the end time of the previous job, on the same machine : */
		long int previousJobEndTime = previousMachineEndTime[1];


		for (j = 2; j <= partialJobNb; ++j)
		{
			jobNumber = sol[j];

			if (previousMachineEndTime[j] > previousJobEndTime)
			{
				previousMachineEndTime[j] = previousMachineEndTime[j] + processingTimesMatrix[jobNumber][m];
				previousJobEndTime = previousMachineEndTime[j];
			}
			else
			{
				previousJobEndTime += processingTimesMatrix[jobNumber][m];
				previousMachineEndTime[j] = previousJobEndTime;
			}
		}
	}

	wct = 0;
	for (j = 1; j <= partialJobNb; ++j)
		wct += previousMachineEndTime[j] * priority[sol[j]];

	return wct;
}

long int PfspInstance::computLastingWCT(const vector<int>& sol, int start)
{
	long int wct = 0;

	//Recompute the WCT starting from a specified point
	for (int i = start; i <= nbJob; ++i)
	{
		int jobNumber = sol[i];
		for (int j = 1; j <= nbMac; ++j)
		{
			//Store every step in the matrix
			matrixWCT[i][j] = max(matrixWCT[i][j - 1], matrixWCT[i - 1][j]) + processingTimesMatrix[jobNumber][j];
		}
	}
	for (int k = 1; k <= nbJob; ++k)
		wct += matrixWCT[k][nbMac] * priority[sol[k]];
	return wct;
}
